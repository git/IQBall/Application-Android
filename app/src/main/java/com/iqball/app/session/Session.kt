package com.iqball.app.session

interface Session {
    val auth: Authentication?
}