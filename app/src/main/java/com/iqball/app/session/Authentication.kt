package com.iqball.app.session
typealias Token = String

data class Authentication(val token: String, val expirationDate: Long)
