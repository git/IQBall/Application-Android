package com.iqball.app.net

import arrow.core.Either
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

class EitherCallAdapterFactory : CallAdapter.Factory() {
    override fun get(
        returnType: Type,
        annotations: Array<out Annotation>,
        retrofit: Retrofit
    ): CallAdapter<*, *>? {
        if (returnType !is ParameterizedType) {
            return null
        }
        if (returnType.rawType != Call::class.java) {
            return null
        }
        val eitherType = returnType.actualTypeArguments.first()
        if (eitherType !is ParameterizedType || eitherType.rawType != Either::class.java) {
            return null
        }
        return object : CallAdapter<Any, Any> {
            override fun responseType(): Type = returnType

            override fun adapt(call: Call<Any>): EitherCall<Any, Any> {
                return EitherCall(retrofit, eitherType, call)
            }
        }
    }

    companion object {
        fun create() = EitherCallAdapterFactory()
    }
}