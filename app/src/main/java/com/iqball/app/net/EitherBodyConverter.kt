package com.iqball.app.net

import arrow.core.Either
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

class EitherBodyConverter : Converter.Factory() {
    override fun responseBodyConverter(
        type: Type,
        annotations: Array<out Annotation>,
        retrofit: Retrofit
    ): Converter<ResponseBody, *>? {

        if (type !is ParameterizedType) {
            return null
        }
        if (type.rawType != Call::class.java) {
            return null
        }
        val eitherType = type.actualTypeArguments.first()
        if (eitherType !is ParameterizedType || eitherType.rawType != Either::class.java) {
            return null
        }

        val eitherRightType = eitherType.actualTypeArguments[1]
        return retrofit.nextResponseBodyConverter<Any>(this, eitherRightType, annotations)
    }

    companion object {
        fun create() = EitherBodyConverter()
    }

}