package com.iqball.app.net.service

import arrow.core.Either

typealias ErrorResponseResult = Map<String, Array<String>>
typealias APIResult<R> = Either<ErrorResponseResult, R>

interface IQBallService : AuthService, UserService, TacticService