package com.iqball.app.net.service

import com.iqball.app.model.tactic.StepContent
import com.iqball.app.model.tactic.StepNodeInfo
import com.iqball.app.session.Token
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path

interface TacticService {

    data class GetTacticInfoResponse(
        val id: Int,
        val name: String,
        val courtType: String,
        val creationDate: Long
    )

    @GET("tactics/{tacticId}")
    suspend fun getTacticInfo(
        @Header("Authorization") auth: Token,
        @Path("tacticId") tacticId: Int
    ): APIResult<GetTacticInfoResponse>

    data class GetTacticStepsTreeResponse(
        val root: StepNodeInfo
    )

    @GET("tactics/{tacticId}/tree")
    suspend fun getTacticStepsTree(
        @Header("Authorization") auth: Token,
        @Path("tacticId") tacticId: Int
    ): APIResult<GetTacticStepsTreeResponse>



    @GET("tactics/{tacticId}/steps/{stepId}")
    suspend fun getTacticStepContent(
        @Header("Authorization") auth: Token,
        @Path("tacticId") tacticId: Int,
        @Path("stepId") stepId: Int
    ): APIResult<StepContent>

}