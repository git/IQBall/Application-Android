package com.iqball.app.net.service

import kotlinx.serialization.Serializable
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthService {

    @Serializable
    data class AuthResponse(val token: String, val expirationDate: Long)

    @Serializable
    data class RegisterRequest(val username: String, val email: String, val password: String)

    @POST("auth/register")
    suspend fun register(@Body req: RegisterRequest): APIResult<AuthResponse>

    data class LoginRequest(val email: String, val password: String)

    @POST("auth/token")
    suspend fun login(@Body req: LoginRequest): APIResult<AuthResponse>


}