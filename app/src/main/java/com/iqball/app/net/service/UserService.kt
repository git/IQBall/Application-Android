package com.iqball.app.net.service

import com.iqball.app.model.Tactic
import com.iqball.app.model.Team
import retrofit2.http.GET
import retrofit2.http.Header

interface UserService {
    data class UserDataResponse(val teams: List<Team>, val tactics: List<Tactic>)

    @GET("user-data")
    suspend fun getUserData(@Header("Authorization") auth: String): APIResult<UserDataResponse>
}