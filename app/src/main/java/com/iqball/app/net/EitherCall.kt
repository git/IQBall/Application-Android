package com.iqball.app.net

import arrow.core.Either
import okhttp3.Request
import okio.Timeout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType

class EitherCall<L, R>(
    private val retrofit: Retrofit,
    private val eitherType: ParameterizedType,
    private val delegate: Call<Any>
) : Call<Either<L, R>> {
    override fun enqueue(callback: Callback<Either<L, R>>) {
        delegate.enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                val result: Either<L, R> = if (response.isSuccessful) {
                    Either.Right(response.body()!! as R)
                } else {
                    val leftType = eitherType.actualTypeArguments[0]
                    val converter = retrofit.nextResponseBodyConverter<L>(null, leftType, arrayOf())
                    val result = converter.convert(response.errorBody()!!)!!
                    Either.Left(result)
                }
                callback.onResponse(this@EitherCall, Response.success(result))
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                callback.onFailure(this@EitherCall, t)
            }

        })
    }

    override fun clone(): Call<Either<L, R>> = EitherCall(retrofit, eitherType, delegate.clone())

    override fun execute(): Response<Either<L, R>> {
        throw UnsupportedOperationException()
    }

    override fun isExecuted(): Boolean = delegate.isExecuted

    override fun cancel() = delegate.cancel()

    override fun isCanceled(): Boolean = delegate.isCanceled

    override fun request(): Request = delegate.request()

    override fun timeout(): Timeout = delegate.timeout()

}