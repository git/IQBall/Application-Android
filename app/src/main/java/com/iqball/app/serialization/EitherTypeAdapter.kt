package com.iqball.app.serialization

import arrow.core.Either
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonDataException
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.Moshi
import com.squareup.moshi.rawType
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type


private class EitherTypeAdapter(
    val leftType: Class<*>,
    val rightType: Class<*>,
    private val moshi: Moshi
) : JsonAdapter<Either<*, *>>() {

    private val leftJsonType = leftType.getJsonPrimitive()
    private val rightJsonType = rightType.getJsonPrimitive()

    override fun fromJson(reader: JsonReader): Either<*, *>? {

        val valueJsonType = when (val token = reader.peek()) {
            JsonReader.Token.BEGIN_ARRAY -> JsonPrimitiveType.Array
            JsonReader.Token.BEGIN_OBJECT -> JsonPrimitiveType.Object
            JsonReader.Token.STRING -> JsonPrimitiveType.String
            JsonReader.Token.NUMBER -> JsonPrimitiveType.Number
            JsonReader.Token.BOOLEAN -> JsonPrimitiveType.Boolean
            JsonReader.Token.NULL -> {
                reader.nextNull<Any>()
                return null
            }
            else -> throw JsonDataException("unexpected token : $token")
        }

        if (valueJsonType == leftJsonType) {
            val value = moshi.adapter<Any>(leftType).fromJson(reader)
            return Either.Left(value)
        }

        if (valueJsonType == rightJsonType) {
            val value = moshi.adapter<Any>(rightType).fromJson(reader)
            return Either.Right(value)
        }
        throw ClassCastException("Cannot cast a json value of type " + valueJsonType + " as either " + leftType.name.lowercase() + " or " + rightType.name.lowercase())
    }


    override fun toJson(writer: JsonWriter, value: Either<*, *>?) {
        when (value) {
            is Either.Left -> writer.jsonValue(value.value)
            is Either.Right -> writer.jsonValue(value.value)
            null -> writer.nullValue()
        }
    }


}

object EitherTypeAdapterFactory : JsonAdapter.Factory {
    override fun create(
        type: Type,
        annotations: MutableSet<out Annotation>,
        moshi: Moshi
    ): JsonAdapter<*>? {
        if (type !is ParameterizedType)
            return null
        if (type.rawType != Either::class.java)
            return null
        val leftType = type.actualTypeArguments[0].rawType
        val rightType = type.actualTypeArguments[1].rawType
        if (leftType.getJsonPrimitive() == rightType.getJsonPrimitive()) {
            throw UnsupportedOperationException("Cannot handle Either types with both sides being object, array, string or number. Provided type is : $type")
        }
        return EitherTypeAdapter(leftType, rightType, moshi)
    }
}

private enum class JsonPrimitiveType {
    Array,
    Object,
    String,
    Number,
    Boolean
}

private fun Class<*>.getJsonPrimitive(): JsonPrimitiveType {
    if (isPrimitive)
        return if (java.lang.Boolean.TYPE == this) JsonPrimitiveType.Boolean else JsonPrimitiveType.Number
    if (isArray)
        return JsonPrimitiveType.Array
    if (this == String::class.java)
        return JsonPrimitiveType.String
    return JsonPrimitiveType.Object
}