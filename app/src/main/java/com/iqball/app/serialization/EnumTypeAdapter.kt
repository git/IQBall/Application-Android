package com.iqball.app.serialization

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.Moshi
import com.squareup.moshi.rawType
import java.lang.reflect.Type
import java.util.NoSuchElementException

class EnumTypeAdapter<E : Enum<E>>(
    values: Map<String, E>,
    private val bindNames: Boolean,
    private val ignoreCase: Boolean,
    private val fallback: E?,
    private val clazz: Class<E>
) : JsonAdapter<E>() {

    private val values = if (ignoreCase) values.mapKeys { it.key.lowercase() } else values

    override fun fromJson(reader: JsonReader): E {
        val value = reader.nextString()
        val key = if (ignoreCase) value.lowercase() else value

        var result = values[key]
        if (result == null && bindNames) {
            result = clazz.enumConstants?.find { it.name.lowercase() == key }
        }

        return result
            ?: fallback
            ?: throw NoSuchElementException("No enum variant matched given values bindings, and no fallback was provided (value = $value, enum type = $clazz)")
    }

    override fun toJson(writer: JsonWriter, value: E?) {
        throw UnsupportedOperationException()
    }

}

class EnumTypeAdapterFactory<E : Enum<E>>(
    private val values: Map<String, E>,
    private val bindNames: Boolean,
    private val ignoreCase: Boolean,
    private val fallback: E?,
    private val clazz: Class<E>
) : JsonAdapter.Factory {
    override fun create(
        type: Type,
        annotations: MutableSet<out Annotation>,
        moshi: Moshi
    ): JsonAdapter<*>? {
        if (type.rawType != clazz)
            return null

        return EnumTypeAdapter(values, bindNames, ignoreCase, fallback, clazz)
    }


    companion object {

        class Builder<E : Enum<E>>(val values: MutableMap<String, E> = HashMap()) {
            infix fun String.means(e: E) {
                values[this] = e
            }
        }

        inline fun <reified E : Enum<E>> create(
            ignoreCase: Boolean = false,
            bindNames: Boolean = true,
            fallback: E? = null,
            build: Builder<E>.() -> Unit = {}
        ): EnumTypeAdapterFactory<E> {
            val builder = Builder<E>()
            build(builder)
            return EnumTypeAdapterFactory(builder.values, bindNames, ignoreCase, fallback, E::class.java)
        }

    }

}