package com.iqball.app.domains

import arrow.core.merge
import com.iqball.app.geo.Vector
import com.iqball.app.model.tactic.BallComponent
import com.iqball.app.model.tactic.FixedPosition
import com.iqball.app.model.tactic.MalformedStepContentException
import com.iqball.app.model.tactic.PhantomComponent
import com.iqball.app.model.tactic.PlayerComponent
import com.iqball.app.model.tactic.PlayerInfo
import com.iqball.app.model.tactic.PlayerLike
import com.iqball.app.model.tactic.RelativePositioning
import com.iqball.app.model.tactic.StepComponent
import com.iqball.app.model.tactic.StepContent

/**
 * Converts the phantom's [Positioning] to a XY Position
 * if the phantom is a [RelativePositioning], the XY coords are determined
 * using the attached component, and by expecting that there is an action on the attached component that
 * targets the given phantom.
 * If so, then the position is determined by projecting the attached component's position, and the direction
 * of the action's last segment.
 * @throws MalformedStepContentException if the step content contains incoherent data
 */
fun computePhantomPosition(phantom: PhantomComponent, content: StepContent): Vector {
    return when (val pos = phantom.pos) {
        is FixedPosition -> pos.toPos()

        is RelativePositioning -> {
            val phantomBefore = getPlayerBefore(phantom, content)!!

            val referentId = pos.attach
            val actions = phantomBefore.actions
            val linkAction = actions.find { it.target.isLeft(referentId::equals) }
                ?: throw MalformedStepContentException("phantom ${phantom.id} is casted by ${phantom}, but there is no action between them.")

            val segments = linkAction.segments
            val lastSegment = segments.last()

            val referent = content.findComponent<StepComponent>(referentId)!!
            val referentPos = computeComponentPosition(referent, content)
            val directionalPos = lastSegment.controlPoint
                ?: segments.elementAtOrNull(segments.size - 2)
                    ?.next
                    ?.mapLeft { computeComponentPosition(content.findComponent(it)!!, content) }
                    ?.merge()
                ?: computeComponentPosition(phantomBefore, content)

            val axisSegment = (directionalPos - referentPos)
            val segmentLength = axisSegment.norm()
            val projectedVector = Vector(
                x = (axisSegment.x / segmentLength) * 0.05F,
                y = (axisSegment.y / segmentLength) * 0.05F,
            )

            referentPos + projectedVector
        }
    }


}

fun computeComponentPosition(component: StepComponent, content: StepContent): Vector =
    when (component) {
        is PhantomComponent -> computePhantomPosition(component, content)
        is PlayerComponent -> component.pos.toPos()
        is BallComponent -> component.pos.toPos()
    }


fun getPlayerBefore(phantom: PhantomComponent, content: StepContent): PlayerLike? {
    val origin = content.findComponent<PlayerComponent>(phantom.originPlayerId)!!
    val items = origin.path!!.items
    val phantomIdx = items.indexOf(phantom.id)
    if (phantomIdx == -1)
        throw MalformedStepContentException("phantom player is not registered it its origin's path")
    if (phantomIdx == 0)
        return origin
    return content.findComponent<PhantomComponent>(items[phantomIdx - 1])
}

fun getPlayerInfo(player: PlayerLike, content: StepContent): PlayerInfo {

    return when (player) {
        is PlayerComponent -> PlayerInfo(
            player.team,
            player.role,
            false,
            player.pos.toPos(),
            player.id,
            player.actions,
            player.ballState
        )

        is PhantomComponent -> {
            val origin = content.findComponent<PlayerComponent>(player.originPlayerId)!!
            val pos = computePhantomPosition(player, content)

            PlayerInfo(
                origin.team,
                origin.role,
                true,
                pos,
                player.id,
                player.actions,
                player.ballState
            )
        }
    }


}