package com.iqball.app.domains

import com.iqball.app.model.tactic.StepNodeInfo


fun getStepName(root: StepNodeInfo, step: Int): String {
    var ord = 1
    val nodes = mutableListOf(root)
    while (nodes.isNotEmpty()) {
        val node = nodes.removeFirst()

        if (node.id == step) break

        ord += 1
        nodes.addAll(node.children.reversed())
    }

    return ord.toString()
}