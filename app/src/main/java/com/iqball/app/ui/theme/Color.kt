package com.iqball.app.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)

val black = Color(0xFF191A21)
val orange = Color(0xFFFFA239)
val blue = Color(0xFF0D6EFD)
val grey = Color(0xFF282A36)
val back = Color(0xFFf8f8f8)
val borderCard = Color(0xFFDADCE0)

val Allies = Color(0xFF64e4f5)
val Opponents = Color(0xFFf59264)
val BallColor = Color(0XFFc5520d)

val StepNode = Color(0xFF2AC008)
val SelectedStepNode = Color(0xFF213519)
