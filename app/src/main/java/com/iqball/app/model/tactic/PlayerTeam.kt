package com.iqball.app.model.tactic

enum class PlayerTeam {
    Allies,
    Opponents
}