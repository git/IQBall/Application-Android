package com.iqball.app.model.tactic

import arrow.core.Either
import arrow.core.NonEmptyList
import com.iqball.app.geo.Vector


enum class ActionType {
    Screen,
    Dribble,
    Move,
    Shoot
}

data class Segment(
    val next: Either<ComponentId, Vector>,
    val controlPoint: Vector?
)

data class Action(
    val type: ActionType,
    val target: Either<ComponentId, Vector>,
    val segments: List<Segment>
)