package com.iqball.app.model

data class Tactic (
    val id: Int,
    val name: String,
    val ownerId: Int,
    val courtType: String,
    val creationDate: Long
)