package com.iqball.app.model.tactic

enum class CourtType {
    Plain,
    Half
}