package com.iqball.app.model.tactic

data class MovementPath(val items: List<ComponentId>)