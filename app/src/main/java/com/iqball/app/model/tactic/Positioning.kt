package com.iqball.app.model.tactic

import com.iqball.app.geo.Pos

sealed interface Positioning
data class RelativePositioning(val attach: ComponentId) : Positioning
data class FixedPosition(val x: Float, val y: Float) : Positioning {
    fun toPos() = Pos(x, y)
}
