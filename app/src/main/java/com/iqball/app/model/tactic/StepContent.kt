package com.iqball.app.model.tactic

import java.lang.RuntimeException

data class StepContent(val components: List<StepComponent>) {
    inline fun <reified C: StepComponent> findComponent(id: String): C? {
        val value = components.find { it.id == id } ?: return null
        if (!C::class.java.isAssignableFrom(value.javaClass))
            return null
        return value as C
    }
}


class MalformedStepContentException(msg: String, cause: Throwable? = null): RuntimeException(msg, cause)