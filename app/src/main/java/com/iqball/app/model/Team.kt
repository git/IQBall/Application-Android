package com.iqball.app.model;

data class Team (
    val id: Int,
    val name: String,
    val picture: String,
    val mainColor: String,
    val secondColor: String
)