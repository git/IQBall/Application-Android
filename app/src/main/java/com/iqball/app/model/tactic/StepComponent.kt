package com.iqball.app.model.tactic

typealias ComponentId = String


sealed interface StepComponent {
    val id: ComponentId
    val actions: List<Action>
}

sealed interface PositionableComponent<P : Positioning> {
    val pos: P
}

sealed interface PlayerLike : PositionableComponent<Positioning>, StepComponent {
    val ballState: BallState
}

data class PlayerComponent(
    val path: MovementPath?,
    val team: PlayerTeam,
    val role: String,
    override val ballState: BallState,
    override val pos: FixedPosition,
    override val id: ComponentId,
    override val actions: List<Action>,
) : PlayerLike, StepComponent

data class PhantomComponent(
    val attachedTo: ComponentId?,
    val originPlayerId: ComponentId,
    override val ballState: BallState,
    override val pos: Positioning,
    override val id: ComponentId,
    override val actions: List<Action>
) : PlayerLike, StepComponent

data class BallComponent(
    override val id: ComponentId,
    override val actions: List<Action>,
    override val pos: FixedPosition
) : StepComponent, PositionableComponent<FixedPosition>
