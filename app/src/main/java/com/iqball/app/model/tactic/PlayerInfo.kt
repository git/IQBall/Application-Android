package com.iqball.app.model.tactic

import com.iqball.app.geo.Vector

data class PlayerInfo(
    val team: PlayerTeam,
    val role: String,
    val isPhantom: Boolean,
    val pos: Vector,
    val id: ComponentId,
    val actions: List<Action>,
    val ballState: BallState
)