package com.iqball.app.model

import com.iqball.app.model.tactic.CourtType
import java.time.LocalDateTime

data class TacticInfo(
    val id: Int,
    val name: String,
    val type: CourtType,
    val creationDate: LocalDateTime
)
