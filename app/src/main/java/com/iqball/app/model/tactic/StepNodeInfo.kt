package com.iqball.app.model.tactic

data class StepNodeInfo(val id: Int, val children: List<StepNodeInfo>)


fun getParent(root: StepNodeInfo, child: Int): StepNodeInfo? {
    for (children in root.children) {
        if (children.id == child) {
            return root
        }
        val result = getParent(children, child)
        if (result != null)
            return result
    }

    return null
}