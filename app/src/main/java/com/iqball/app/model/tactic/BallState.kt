package com.iqball.app.model.tactic

enum class BallState {
    None,
    HoldsOrigin,
    HoldsByPass,
    Passed,
    PassedOrigin;


    fun hasBall() = this != None

}