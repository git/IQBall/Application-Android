package com.iqball.app.component

import androidx.compose.foundation.layout.size
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.iqball.app.R
import com.iqball.app.ui.theme.BallColor

const val BallPieceDiameterDp = 20

@Composable
fun BallPiece(modifier: Modifier = Modifier) {
    Icon(
        painter = painterResource(R.drawable.ball),
        contentDescription = "ball",
        tint = BallColor,
        modifier = modifier.size(BallPieceDiameterDp.dp)
    )
}