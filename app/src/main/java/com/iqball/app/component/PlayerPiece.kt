package com.iqball.app.component

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.iqball.app.model.tactic.PlayerInfo
import com.iqball.app.model.tactic.PlayerTeam
import com.iqball.app.ui.theme.Allies
import com.iqball.app.ui.theme.Opponents

const val PlayerPieceDiameterDp = 25

@Composable
fun PlayerPiece(player: PlayerInfo, modifier: Modifier = Modifier, isFromParent: Boolean) {

    val color = if (isFromParent) Color.LightGray else if (player.team === PlayerTeam.Allies) Allies else Opponents

    return Surface(
        shape = CircleShape,
        border = if (player.ballState.hasBall()) BorderStroke(2.dp, Color.Black) else null,
        modifier = modifier
            .alpha(if (player.isPhantom) .5F else 1F)
    ) {
        Text(
            text = player.role,
            textAlign = TextAlign.Center,
            color = Color.Black,
            modifier = Modifier
                .background(color)
                .size(PlayerPieceDiameterDp.dp)
        )
    }
}