package com.iqball.app.component

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateMapOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.boundsInRoot
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.chihsuanwu.freescroll.freeScroll
import com.chihsuanwu.freescroll.rememberFreeScrollState
import com.iqball.app.domains.getStepName
import com.iqball.app.model.tactic.StepNodeInfo
import com.iqball.app.ui.theme.SelectedStepNode
import com.iqball.app.ui.theme.StepNode

@Composable
fun StepsTree(root: StepNodeInfo, selectedNodeId: Int, onNodeSelected: (StepNodeInfo) -> Unit) {

    val scrollState = rememberFreeScrollState()
    val nodesOffsets = remember { mutableStateMapOf<StepNodeInfo, Rect>() }
    var globalOffset by remember { mutableStateOf(Offset.Zero) }

    Box(
        modifier = Modifier
            .fillMaxSize()
            .freeScroll(scrollState)
            .onGloballyPositioned {
                if (globalOffset == Offset.Zero)
                    globalOffset = it.boundsInRoot().topLeft
            }
            .drawWithContent {

                if (nodesOffsets.isEmpty()) {
                    drawContent()
                    return@drawWithContent
                }

                val toDraw = mutableListOf(root)
                while (toDraw.isNotEmpty()) {
                    val parent = toDraw.removeLast()
                    val parentCenter = nodesOffsets[parent]!!.center - globalOffset
                    for (children in parent.children) {
                        val childrenCenter = nodesOffsets[children]!!.center - globalOffset
                        drawLine(
                            Color.Black,
                            start = parentCenter,
                            end = childrenCenter,
                            strokeWidth = 5F
                        )
                        toDraw += children
                    }
                }

                drawContent()

            },
        contentAlignment = Alignment.TopCenter
    ) {
        StepsTreeContent(root, root, selectedNodeId, onNodeSelected, nodesOffsets)
    }
}

@Composable
private fun StepsTreeContent(
    root: StepNodeInfo,
    node: StepNodeInfo,
    selectedNodeId: Int,
    onNodeSelected: (StepNodeInfo) -> Unit,
    nodesOffsets: MutableMap<StepNodeInfo, Rect>
) {

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
    ) {
        StepPiece(
            name = getStepName(root, node.id),
            node = node,
            isSelected = selectedNodeId == node.id,
            onNodeSelected = { onNodeSelected(node) },
            modifier = Modifier
                .padding(10.dp)
                .onGloballyPositioned {
                    if (!nodesOffsets.containsKey(node))
                        nodesOffsets[node] = it.boundsInRoot()
                }
        )

        Row(
            modifier = Modifier
                .padding(top = 50.dp)
        ) {
            for (children in node.children) {
                StepsTreeContent(
                    root = root,
                    node = children,
                    selectedNodeId = selectedNodeId,
                    onNodeSelected = onNodeSelected,
                    nodesOffsets = nodesOffsets
                )
            }
        }
    }
}

@Composable
fun StepPiece(
    name: String,
    node: StepNodeInfo,
    isSelected: Boolean,
    onNodeSelected: () -> Unit,
    modifier: Modifier = Modifier
) {
    val color = if (isSelected) SelectedStepNode else StepNode

    return Surface(
        shape = CircleShape,
        modifier = modifier.clickable {
            onNodeSelected()
        }
    ) {
        Text(
            text = name,
            textAlign = TextAlign.Center,
            color = if (isSelected) Color.White else Color.Black,
            modifier = Modifier
                .background(color)
                .size(PlayerPieceDiameterDp.dp)
        )
    }
}