package com.iqball.app.component

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.layout.requiredWidth
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.boundsInParent
import androidx.compose.ui.layout.boundsInRoot
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import arrow.core.getOrNone
import com.iqball.app.R
import com.iqball.app.domains.getPlayerInfo
import com.iqball.app.geo.toVector
import com.iqball.app.model.tactic.BallComponent
import com.iqball.app.model.tactic.ComponentId
import com.iqball.app.model.tactic.CourtType
import com.iqball.app.model.tactic.PlayerLike
import com.iqball.app.model.tactic.StepContent
import net.engawapg.lib.zoomable.ZoomState
import net.engawapg.lib.zoomable.zoomable

data class BasketCourtStates(
    val stepComponentsOffsets: MutableMap<ComponentId, Offset>,
    val parentComponentsOffsets: MutableMap<ComponentId, Offset>,
    val courtArea: MutableState<Rect>,
    val zoomState: ZoomState
)

@Composable
fun BasketCourt(
    content: StepContent,
    parentContent: StepContent?,
    type: CourtType,
    modifier: Modifier,
    state: BasketCourtStates
) {
    val courtImg = when (type) {
        CourtType.Plain -> R.drawable.plain_court
        CourtType.Half -> R.drawable.half_court
    }

    var courtArea by state.courtArea
    val zoomState = state.zoomState


    Box(
        modifier = modifier
            .background(Color.LightGray)
            .fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Box(
            modifier = Modifier
                .width(IntrinsicSize.Min)
                .zoomable(zoomState),
            contentAlignment = Alignment.Center,
        ) {
            Image(
                painter = painterResource(id = courtImg),
                contentDescription = "court",
                modifier = Modifier
                    .background(Color.White)
                    .onGloballyPositioned {
                        if (courtArea == Rect.Zero)
                            courtArea = it.boundsInRoot()
                    }
            )

            CourtContent(
                courtArea = courtArea,
                content = content,
                offsets = state.stepComponentsOffsets,
                isFromParent = false
            )
            if (parentContent != null) {
                CourtContent(
                    courtArea = courtArea,
                    content = parentContent,
                    offsets = state.parentComponentsOffsets,
                    isFromParent = true
                )
            }
        }
    }
}

@Composable
private fun CourtContent(
    courtArea: Rect,
    content: StepContent,
    offsets: MutableMap<ComponentId, Offset>,
    isFromParent: Boolean
) {
    val playersPixelsRadius = LocalDensity.current.run { PlayerPieceDiameterDp.dp.toPx() / 2 }

    val width = LocalDensity.current.run { courtArea.width.toDp() }
    val height = LocalDensity.current.run { courtArea.height.toDp() }

    Box(
        modifier = Modifier
            .requiredWidth(width)
            .requiredHeight(height)
            .drawWithContent {
                val relativeOffsets =
                    offsets.mapValues { (it.value).toVector() }
                drawActions(
                    this,
                    content,
                    relativeOffsets,
                    courtArea,
                    playersPixelsRadius,
                    if (isFromParent) Color.Gray else Color.Black
                )
                drawContent()
            }
    ) {


        for (component in content.components) {
            val componentModifier = Modifier
                .onGloballyPositioned {
                    if (!offsets.getOrNone(component.id).isSome { it != Offset.Zero })
                        offsets[component.id] = it.boundsInParent().center
                }
            when (component) {
                is PlayerLike -> {
                    val info = getPlayerInfo(component, content)
                    PlayerPiece(
                        player = info,
                        isFromParent = isFromParent,
                        modifier = componentModifier
                            .align(info.pos.toBiasAlignment())
                    )
                }

                is BallComponent -> BallPiece(
                    modifier = componentModifier
                        .align(
                            component.pos
                                .toPos()
                                .toBiasAlignment()
                        )
                )
            }
        }
    }
}