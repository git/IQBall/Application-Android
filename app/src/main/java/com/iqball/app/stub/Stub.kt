package com.iqball.app.stub

import com.iqball.app.model.Tactic
import com.iqball.app.model.Team


private fun getStubTeam(): ArrayList<Team> {
    val teams = ArrayList<Team>()
    teams.addAll(
        listOf(
            Team(
                1,
                "equipe1",
                "https://www.shutterstock.com/image-vector/batman-logo-icon-vector-template-600nw-1998917738.jpg",
                "#4500FF",
                "#456789"
            ),
            Team(
                2,
                "equipe2",
                "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/2f899b52-daf8-4098-83fe-5c5e27b69915/d4s4nzj-5f915488-7462-4908-b3c5-1605b0e4dc32.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzJmODk5YjUyLWRhZjgtNDA5OC04M2ZlLTVjNWUyN2I2OTkxNVwvZDRzNG56ai01ZjkxNTQ4OC03NDYyLTQ5MDgtYjNjNS0xNjA1YjBlNGRjMzIuanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.KqdQgobH9kzyMIeYIneNdyWgKTpGbztwSKqK5pO3YYs",
                "121212",
                "#564738"
            ),
            Team(
                3,
                "equipe3",
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1jiizrhhGsr48WrxxBbDpkFrRKeAYlGgcNQ&usqp=CAU",
                "#987654",
                "121212"
            ),
            Team(
                4,
                "equipe4",
                "https://www.shutterstock.com/image-vector/batman-logo-icon-vector-template-600nw-1998917738.jpg",
                "121212",
                "121212"
            )
        )
    )
    return teams
}

private fun getStubTactic(): ArrayList<Tactic> {
    val tactics = ArrayList<Tactic>()
    tactics.addAll(
        listOf(
            Tactic(1, "Test", 1, "testType", 1),
            Tactic(2, "Test2", 1, "testType", 1),
            Tactic(3, "Test3", 4, "test23Type", 1),
            Tactic(3, "Test6", 4, "test23Type", 1),
            Tactic(1, "Test", 1, "testType", 1),
            Tactic(2, "Test2", 1, "testType", 1),
            Tactic(3, "Test3", 4, "test23Type", 1),
            Tactic(3, "Test6", 4, "test23Type", 1),
            Tactic(1, "Test", 1, "testType", 1),
            Tactic(2, "Test2", 1, "testType", 1),
            Tactic(3, "Test3", 4, "test23Type", 1),
            Tactic(3, "Test6", 4, "test23Type", 1),
            Tactic(1, "Test", 1, "testType", 1),
            Tactic(2, "Test2", 1, "testType", 1),
            Tactic(3, "Test3", 4, "test23Type", 1),
            Tactic(3, "Test6", 4, "test23Type", 1),
            Tactic(1, "Test", 1, "testType", 1),
            Tactic(2, "Test2", 1, "testType", 1),
            Tactic(3, "Test3", 4, "test23Type", 1),
            Tactic(3, "Test6", 4, "test23Type", 1)
        )
    )

    return tactics
}
