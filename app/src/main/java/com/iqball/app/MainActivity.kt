package com.iqball.app

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier

import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.iqball.app.model.tactic.ActionType
import com.iqball.app.model.tactic.BallComponent
import com.iqball.app.model.tactic.BallState
import com.iqball.app.model.tactic.FixedPosition
import com.iqball.app.model.tactic.PhantomComponent
import com.iqball.app.model.tactic.PlayerComponent
import com.iqball.app.model.tactic.PlayerTeam
import com.iqball.app.model.tactic.Positioning
import com.iqball.app.model.tactic.RelativePositioning
import com.iqball.app.model.tactic.StepComponent
import com.iqball.app.net.EitherBodyConverter
import com.iqball.app.net.EitherCallAdapterFactory
import com.iqball.app.net.service.IQBallService
import com.iqball.app.page.HomePage
import com.iqball.app.page.LoginPage
import com.iqball.app.page.RegisterPage
import com.iqball.app.serialization.EitherTypeAdapterFactory
import com.iqball.app.serialization.EnumTypeAdapterFactory
import com.iqball.app.session.DataSession
import com.iqball.app.session.Session
import com.iqball.app.ui.theme.IQBallTheme
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.create

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val moshi = Moshi.Builder()
            .add(EitherTypeAdapterFactory)
            .add(
                PolymorphicJsonAdapterFactory.of(StepComponent::class.java, "type")
                    .withSubtype(PlayerComponent::class.java, "player")
                    .withSubtype(PhantomComponent::class.java, "phantom")
                    .withSubtype(BallComponent::class.java, "ball")
            )
            .add(
                PolymorphicJsonAdapterFactory.of(Positioning::class.java, "type")
                    .withSubtype(FixedPosition::class.java, "fixed")
                    .withSubtype(RelativePositioning::class.java, "follows")
            )
            .add(EnumTypeAdapterFactory.create<PlayerTeam>(true))
            .add(EnumTypeAdapterFactory.create<BallState>(true) {
                "HOLDS_ORIGIN" means BallState.HoldsOrigin
                "HOLDS_BY_PASS" means BallState.HoldsByPass
                "PASSED_ORIGIN" means BallState.PassedOrigin
            })
            .add(EnumTypeAdapterFactory.create<ActionType>(true))
            .add(KotlinJsonAdapterFactory())
            .build()

        val retrofit = Retrofit.Builder()
            .addConverterFactory(EitherBodyConverter.create())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(EitherCallAdapterFactory.create())
            .baseUrl("https://iqball.maxou.dev/api/dotnet-master/")
            .client(
                OkHttpClient.Builder()
                    .addInterceptor { it.proceed(it.request()) }
                    .build()
            )
            .build()

        val service = retrofit.create<IQBallService>()

        setContent {
            IQBallTheme(darkTheme = false, dynamicColor = false) {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    val sessionState = remember { mutableStateOf<Session>(DataSession()) }
                    App(service, sessionState)
                }
            }
        }
    }
}

@Composable
fun App(service: IQBallService, sessionState: MutableState<Session>) {
    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = "login") {
        composable("login") {
            LoginPage(
                service = service,
                onLoginSuccess = { auth ->
                    Log.i("ZIZI", "auth : ${auth}")
                    sessionState.value = DataSession(auth)
                    navController.navigate("home")
                    Log.i("ZIZI", "auth : ${auth}")
                },
                onNavigateToRegister = {
                    navController.navigate("register")
                }
            )
        }
        composable("register") {
            RegisterPage(
                service = service,
                onRegisterSuccess = { auth ->
                    sessionState.value = DataSession(auth)
                    navController.navigate("home")
                },
                onNavigateToLogin = {
                    navController.navigate("login")
                }
            )
        }
        composable("home") {
            HomePage(service = service, sessionState.value.auth!!)
        }
    }
}