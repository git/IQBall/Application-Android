package com.iqball.app.geo

import androidx.compose.ui.BiasAlignment
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Rect
import kotlin.math.atan2
import kotlin.math.sqrt

typealias Pos = Vector

data class Vector(val x: Float, val y: Float) {
    fun toBiasAlignment(): BiasAlignment =
        BiasAlignment((x * 2 - 1), (y * 2 - 1))

    infix operator fun minus(other: Vector) = Vector(x - other.x, y - other.y)
    infix operator fun plus(other: Vector) = Vector(x + other.x, y + other.y)
    infix operator fun div(other: Vector) = Vector(x / other.x, y / other.y)
    infix operator fun div(n: Float) = Vector(x / n, y / n)

    infix operator fun times(other: Vector) = Vector(x * other.x, y * other.y)
    infix operator fun times(n: Float) = Vector(x * n, y * n)
    infix operator fun times(n: Int) = Vector(x * n, y * n)


    fun angleRadWith(other: Vector): Float {
        val (x, y) = this - other
        return atan2(x, y)
    }

    fun angleDegWith(other: Vector): Float = (angleRadWith(other) * (180 / Math.PI)).toFloat()


    fun distanceFrom(other: Vector) =
        sqrt(((x - other.x) * (x - other.x) + (y - other.y) * (y - other.y)))

    fun norm() = NullVector.distanceFrom(this)

    fun posWithinArea(area: Rect) = Vector(x * area.width, y * area.height)

    fun toOffset() = Offset(x, y)
}

val NullVector = Vector(0F, 0F)

fun Offset.toVector() = Vector(x, y)