package com.iqball.app.page

import com.iqball.app.model.Tactic
import com.iqball.app.model.Team
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.Composable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.staggeredgrid.LazyVerticalStaggeredGrid
import androidx.compose.foundation.lazy.staggeredgrid.StaggeredGridCells
import androidx.compose.foundation.lazy.staggeredgrid.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.core.graphics.toColorInt
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.TopAppBarDefaults.pinnedScrollBehavior
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.TextUnit
import arrow.core.Either
import coil.compose.AsyncImage
import com.iqball.app.net.service.AuthService
import com.iqball.app.net.service.IQBallService
import com.iqball.app.net.service.UserService
import com.iqball.app.session.Authentication
import kotlinx.coroutines.runBlocking
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HomePage(service: IQBallService, auth: Authentication) {
    val tactics: List<Tactic>
    val teams: List<Team>
    var invalid = false

    val data = getDataFromApi(service, auth)
    if (data == null) {
        tactics = listOf<Tactic>()
        teams = listOf<Team>()
        invalid = true

    } else {
        tactics = data.tactics
        teams = data.teams
    }
    val scrollBehavior = pinnedScrollBehavior(rememberTopAppBarState())
    Scaffold(
        modifier = Modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
        topBar = {
            CenterAlignedTopAppBar(
                colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = MaterialTheme.colorScheme.primary,
                    titleContentColor = MaterialTheme.colorScheme.secondary,
                ),
                title = {
                    Text(
                        "IQBall",
                        fontSize = 40.sp,
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis
                    )
                },
                scrollBehavior = scrollBehavior
            )
        },
    ) { innerPadding ->
        Body(innerPadding, tactics, teams, invalid)
    }
}

@Composable
private fun Body(
    padding: PaddingValues,
    tactics: List<Tactic>,
    teams: List<Team>,
    invalid: Boolean
) {
    Column(
        modifier = Modifier
            .padding(padding)
    ) {
        val selectedTab = remember { mutableIntStateOf(0) }
        val tabs = listOf<Pair<String, @Composable () -> Unit>>(
            Pair("Espace personnel") {
                ListComponentCard(tactics) { tactic ->
                    TacticCard(tactic = tactic)
                }
            },
            Pair("Mes équipes") {
                ListComponentCard(teams) { team ->
                    TeamCard(team = team)
                }
            }
        )
        TabsSelector(tabsTitles = tabs.map { it.first }, selectedIndex = selectedTab)
        if (!invalid) {
            tabs[selectedTab.intValue].second()
            return
        }


        TextCentered(
            text = "Erreur : Aucune connexion internet. Veillez activer votre connexion internet puis relancer l'application",
            fontSize = 20.sp
        )
    }
}

@Composable
private fun TabsSelector(tabsTitles: List<String>, selectedIndex: MutableState<Int>) {
    Row(
        horizontalArrangement = Arrangement.Center,
        modifier = Modifier
            .padding(top = 20.dp, start = 2.dp, end = 2.dp, bottom = 10.dp)
            .fillMaxWidth()
    ) {

        for ((idx, tab) in tabsTitles.withIndex()) {
            TabButton(
                tab,
                fill = idx == selectedIndex.value,
                onClick = { selectedIndex.value = idx }
            )
            if (idx != tabsTitles.size - 1) {
                Spacer(
                    modifier = Modifier
                        .padding(5.dp)
                )
            }
        }

    }
}

@Composable
private fun TabButton(title: String, fill: Boolean, onClick: () -> Unit) {
    val scheme = MaterialTheme.colorScheme
    Button(
        border = BorderStroke(
            1.dp,
            color = scheme.tertiary
        ),
        colors = ButtonDefaults.buttonColors(
            containerColor = if (fill) scheme.tertiary else scheme.background,
            contentColor = if (fill) scheme.background else scheme.tertiary,
        ),
        onClick = onClick
    ) {
        Text(title)
    }
}

@Composable
private fun <C> ListComponentCard(items: List<C>, componentCard: @Composable (C) -> Unit) {
    LazyVerticalStaggeredGrid(
        columns = StaggeredGridCells.Fixed(2),
        modifier = Modifier
            .padding(5.dp),
        content = {
            items(items) { tactic ->
                componentCard(tactic)
            }
        }
    )
}

@Composable
private fun TacticCard(tactic: Tactic) {
    Column(
        modifier = Modifier
            .padding(5.dp)
            .border(
                border = BorderStroke(1.dp, MaterialTheme.colorScheme.outline),
                shape = RoundedCornerShape(8.dp)
            )
            .shadow(1.dp, shape = RoundedCornerShape(8.dp))
            .background(
                color = Color.White
            )
            .padding(15.dp)
    ) {
        Row {
            TextCentered(text = tactic.name, fontSize = 16.sp)
        }
        Row {
            val date = LocalDateTime.ofInstant(
                Instant.ofEpochMilli(tactic.creationDate),
                ZoneId.systemDefault()
            )
            val dateFormatted = date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy kk:mm"))
            TextCentered(
                text = dateFormatted,
                fontSize = 10.sp,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(2.dp)
            )
        }
    }
}

@Composable
private fun TeamCard(team: Team) {
    var mainColor = Color.White
    var secondColor = Color.White
    var validMain = true
    var validSecond = true
    try {
        mainColor = Color(team.mainColor.toColorInt())
    } catch (e: Exception) {
        validMain = false
    }
    try {
        secondColor = Color(team.secondColor.toColorInt())
    } catch (e: Exception) {
        validSecond = false
    }

    Column(
        modifier = Modifier
            .padding(5.dp)
            .border(
                border = BorderStroke(1.dp, MaterialTheme.colorScheme.outline),
                shape = RoundedCornerShape(8.dp)
            )
            .shadow(1.dp, shape = RoundedCornerShape(8.dp))
            .background(
                color = Color.White
            )
            .padding(15.dp)

    ) {
        AsyncImage(
            model = team.picture,
            contentDescription = null,
            modifier = Modifier
                .fillMaxWidth()
                .border(
                    border = BorderStroke(1.dp, MaterialTheme.colorScheme.outline),
                    shape = RectangleShape
                )
        )
        TextCentered(text = team.name)
        Row {
            TeamColorCard("Couleur principale", mainColor, 0.5f)
            TeamColorCard("Couleur secondaire", secondColor)
        }
        if (!validMain || !validSecond) {
            TextCentered(text = "Erreur : Format des couleurs invalides", fontSize = 16.sp)
        }
    }
}

@Composable
private fun TeamColorCard(text: String, color: Color, fraction: Float = 1f) {
    Column(
        modifier = Modifier
            .fillMaxWidth(fraction)
    ) {
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier
                .fillMaxWidth()
                .padding(2.dp)

        ) {
            Canvas(
                modifier = Modifier.size(30.dp),
                onDraw = {
                    drawCircle(color = color)
                }
            )
        }
        TextCentered(text = text, fontSize = 6.sp)
    }
}

@Composable
private fun TextCentered(
    modifier: Modifier = Modifier,
    text: String,
    fontSize: TextUnit = 18.sp,
    fontWeight: FontWeight? = null
) {
    Text(
        text = text,
        modifier = modifier
            .fillMaxWidth(),
        textAlign = TextAlign.Center,
        fontSize = fontSize,
        fontWeight = fontWeight
    )
}

private fun getDataFromApi(
    service: IQBallService,
    auth: Authentication
): UserService.UserDataResponse? {
    var res: UserService.UserDataResponse? = null
    try {
        runBlocking {
            val data = service.getUserData(auth.token)
            when (data) {
                is Either.Left -> null
                is Either.Right -> {
                    res = data.value
                }
            }
        }
        return res
    } catch (error: Exception) {
        return res
    }
}

/*
=======================================================
                    Comment
=======================================================

Managing lists to display with pairs might not be the best thing to do in the context of composable.
We chose to stick with this model due to a lack of time.

We could have also done something like this:

@Composable
fun Body(padding: PaddingValues, tactics: List<Tactic>, teams: List<Team>, invalid: Boolean) {
    Column(...) {
        val selectedTab by remember { mutableIntStateOf(0) }
        val tabs = remember(selectedTab) {
            mutableStateOf(TabsGroup.entries.getOrNull(selectedTab))
        }
        TabsSelector(tabsTitles = TabsGroup.entries.map { it.title }, selectedIndex = selectedTab, )
       	if (!invalid) {
                ListComponentCard {
                    when(selectedTab) {
                        TabsGroup.TEAM ->  {
                            items(tactics) {
                                TacticCard(tactic = it)
                            }
                        }
                        TabsGroup.TACTIC -> ...
                    }
            }
            tabs[selectedTab.intValue].second()
            return
        }

        TextCentered(...)
    }
}

enum class TabsGroup {
    TEAM,
    TACTIC
}

val TabsGroup.title: String
    @Composable
    get() = when(this) {
        TabsGroup.TACTIC -> "Espace personnel"
        TabsGroup.TEAM -> "Mes équipes"
    }

@Composable
fun ListComponentCard(componentCard: LazyStaggeredGridScope.() -> Unit) {
    LazyVerticalStaggeredGrid(
        columns = StaggeredGridCells.Fixed(2),
        modifier = ...,
        content = {
                componentCard()
        }
    )
}

 */