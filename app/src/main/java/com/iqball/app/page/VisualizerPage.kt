package com.iqball.app.page

import android.content.res.Configuration
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateMapOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.zIndex
import arrow.core.Either
import com.iqball.app.R
import com.iqball.app.component.BasketCourt
import com.iqball.app.component.BasketCourtStates
import com.iqball.app.component.StepsTree
import com.iqball.app.model.TacticInfo
import com.iqball.app.model.tactic.ComponentId
import com.iqball.app.model.tactic.CourtType
import com.iqball.app.model.tactic.StepContent
import com.iqball.app.model.tactic.StepNodeInfo
import com.iqball.app.model.tactic.getParent
import com.iqball.app.net.service.TacticService
import com.iqball.app.session.Token
import kotlinx.coroutines.runBlocking
import net.engawapg.lib.zoomable.ZoomState
import net.engawapg.lib.zoomable.rememberZoomState
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId

private data class VisualizerInitialData(
    val info: TacticInfo,
    val rootStep: StepNodeInfo,
)

@Composable
fun VisualizerPage(
    service: TacticService,
    auth: Token,
    tacticId: Int,
) {
    val dataEither = remember { initializeVisualizer(service, auth, tacticId) }
    val showTree = remember { mutableStateOf(true) }

    val (info, stepsTree) = when (dataEither) {
        // On error return a text to print it to the user
        is Either.Left -> return Text(text = dataEither.value)
        is Either.Right -> dataEither.value
    }

    fun getStepContent(step: Int): StepContent = runBlocking {
        val result = service.getTacticStepContent(auth, tacticId, step).onLeft {
            Log.e(
                "received error response from server when retrieving step content: {}",
                it.toString()
            )
        }
        when (result) {
            is Either.Left -> throw Error("Unexpected error")
            is Either.Right -> result.value
        }
    }


    val screenOrientation = LocalConfiguration.current.orientation
    var selectedStepId by rememberSaveable { mutableIntStateOf(stepsTree.id) }
    val (content, parentContent) = remember(selectedStepId) {
        val parentId = getParent(stepsTree, selectedStepId)?.id
        Pair(
            getStepContent(selectedStepId),
            parentId?.let { getStepContent(it) }
        )
    }


    Column {
        VisualizerHeader(title = info.name, showTree)
        when (screenOrientation) {
            Configuration.ORIENTATION_PORTRAIT -> StepsTree(root = stepsTree,
                selectedNodeId = selectedStepId,
                onNodeSelected = { selectedStepId = it.id })

            Configuration.ORIENTATION_LANDSCAPE -> {
                val courtArea = remember { mutableStateOf(Rect.Zero) }
                val stepOffsets =
                    remember(selectedStepId) { mutableStateMapOf<ComponentId, Offset>() }
                val parentOffsets =
                    remember(selectedStepId) { mutableStateMapOf<ComponentId, Offset>() }

                val courtModifier =
                    if (showTree.value) Modifier.width(IntrinsicSize.Min) else Modifier.fillMaxWidth()


                val courtZoomState = remember { ZoomState() }

                Row(modifier = Modifier.background(Color.LightGray)) {
                    BasketCourt(
                        content = content,
                        parentContent,
                        type = info.type,
                        modifier = courtModifier,
                        state = BasketCourtStates(
                            stepOffsets,
                            parentOffsets,
                            courtArea,
                            courtZoomState
                        )
                    )
                    if (showTree.value) {
                        StepsTree(
                            root = stepsTree,
                            selectedNodeId = selectedStepId,
                            onNodeSelected = { selectedStepId = it.id }
                        )
                    }
                }
            }

            else -> throw Exception("Could not determine device's orientation.")
        }
    }

}

@Composable
private fun VisualizerHeader(title: String, showTree: MutableState<Boolean>) {


    Row(
        modifier = Modifier
            .fillMaxWidth()
            .zIndex(10000F)
            .background(Color.White),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        IconButton(onClick = { /*TODO*/ }) {
            Icon(
                imageVector = Icons.Filled.ArrowBack,
                contentDescription = "Back",
                tint = Color.Black
            )
        }

        Text(text = title, color = Color.Black)

        IconButton(onClick = { showTree.value = !showTree.value }) {
            Icon(
                painter = painterResource(id = R.drawable.tree_icon),
                contentDescription = "toggle show tree"
            )
        }
    }
}

private fun initializeVisualizer(
    service: TacticService, auth: Token, tacticId: Int
): Either<String, VisualizerInitialData> {
    val (tacticInfo, tacticTree) = runBlocking {
        val tacticInfo = service.getTacticInfo(auth, tacticId).map {
            TacticInfo(
                id = it.id, name = it.name, type = CourtType.valueOf(
                    it.courtType.lowercase().replaceFirstChar(Char::uppercaseChar)
                ), creationDate = LocalDateTime.ofInstant(
                    Instant.ofEpochMilli(it.creationDate), ZoneId.systemDefault()
                )
            )
        }.onLeft {
            Log.e(
                "received error response from server when retrieving tacticInfo : {}", it.toString()
            )
        }

        val tacticTree = service.getTacticStepsTree(auth, tacticId).map { it.root }.onLeft {
            Log.e(
                "received error response from server when retrieving tactic steps tree: {}",
                it.toString()
            )
        }

        Pair(tacticInfo.getOrNull(), tacticTree.getOrNull())
    }

    if (tacticInfo == null || tacticTree == null) {
        return Either.Left("Unable to retrieve tactic information")
    }


    return Either.Right(VisualizerInitialData(tacticInfo, tacticTree))
}