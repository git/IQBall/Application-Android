package com.iqball.app.page

import android.util.Log
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Button
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Surface
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import arrow.core.Either
import com.iqball.app.net.service.AuthService
import com.iqball.app.session.Authentication
import kotlinx.coroutines.runBlocking

@Composable
fun LoginPage(
    service: AuthService,
    onLoginSuccess: (Authentication) -> Unit,
    onNavigateToRegister: () -> Unit
) {
    var email by remember { mutableStateOf("") }
    var password by remember { mutableStateOf("") }
    var errors by remember { mutableStateOf("") }

    Surface(
        color = Color.White,
        modifier = Modifier.fillMaxSize()
    ) {
        Column(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = "S'identifier",
                fontSize = 28.sp,
                color = Color.Black
            )

            Text(
                text = errors,
                color = Color.Red,
                fontSize = 14.sp,
                modifier = Modifier.padding(vertical = 8.dp)
            )

            Spacer(modifier = Modifier.height(16.dp))
            OutlinedTextField(
                value = email,
                onValueChange = { email = it },
                label = { Text("Email") },
                modifier = Modifier.fillMaxWidth(),
                colors = OutlinedTextFieldDefaults.colors(
                    focusedTextColor = Color.Black,
                    unfocusedTextColor = Color.Black
                )
            )
            Spacer(modifier = Modifier.height(16.dp))
            OutlinedTextField(
                value = password,
                onValueChange = { password = it },
                label = { Text("Password") },
                visualTransformation = PasswordVisualTransformation(),
                modifier = Modifier.fillMaxWidth(),
                colors = OutlinedTextFieldDefaults.colors(
                    focusedTextColor = Color.Black,
                    unfocusedTextColor = Color.Black
                )
            )
            Spacer(modifier = Modifier.height(16.dp))

            Button(onClick = {
                runBlocking {
                    Log.i("ZIZI", "On click wesh")
                    when (val response = service.login(AuthService.LoginRequest(email, password))) {
                        is Either.Left -> {
                            errors = response.value.toList()
                                .flatMap { entry -> entry.second.map { "${entry.first} : ${it}" } }
                                .joinToString("\n")
                        }

                        is Either.Right -> onLoginSuccess(
                            Authentication(
                                response.value.token,
                                response.value.expirationDate.toLong()
                            )
                        )
                    }
                }
            }) {
                Text(text = "Se connecter")
            }
            Spacer(modifier = Modifier.height(16.dp))
            Button(onClick = { onNavigateToRegister() }) {
                Text(text = "Vous n'avez pas de compte ?")
            }
        }
    }
}